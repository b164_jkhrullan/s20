console.log("Hello World again!");

let users = `[
  
  {
    "id": 1,
    "name": "Julina Krizzia Rullan",
    "username": "jkrizziahr",
    "email": "jkrizziahrullan@gmail.com",
    "address": {
      "street": "53 Del Nacia Drive",
      "city": "Quezon City",
      "zipcode": "1116",
      }
    },
    "phone": "8931-1331",
    "company": {
      "name": "DENR EMB-NCR",
    }
  },
  {
    "id": 2,
    "name": "Michael Dave Martinez",
    "username": "davemrtnz_",
    "email": "davemartinez@gmail.com",
    "address": {
      "street": "Carnation Street",
      "city": "San Mateo City",
      "zipcode": "1118",
      }
    },
    "phone": "8931-1336",
    "company": {
      "name": "Landchecker",
    }
  },
  {
      "id": 3,
      "name": "Kaye Dela Cruz",
      "username": "jukahira",
      "email": "jukahiru@gmail.com",
      "address": {
        "street": "Rose Street",
        "city": "Calaoocan City",
        "zipcode": "1117",
        }
      },
      "phone": "8931-1335",
      "company": {
        "name": "DC Inc.",
      }
    },
]`;

console.log(JSON.parse(users));

let product = JSON.stringify({
  name: "Ergonomics Chair",
  category: "Furniture",
  quantity: 8,
  model: 'MK164'
});

console.log(JSON.stringify(product));
